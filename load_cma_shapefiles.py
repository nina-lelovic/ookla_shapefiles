import os
import pandas
import geopandas as gpd
import coloredlogs, logging
import pandas as pd
import sqlalchemy
from snowflake.connector.pandas_tools import pd_writer
from dotenv import load_dotenv
from sqlalchemy import create_engine

load_dotenv()


def setup_logger():
    fmt = '%(asctime)s [%(levelname)s] %(message)s'
    date_fmt = '%Y-%m-%d %H:%M'
    try:
        fmt = '%(asctime)s [%(levelname)s] %(message)s'
        date_fmt = '%Y-%m-%d %H:%M'
        coloredlogs.install(fmt=fmt, datefmt=date_fmt, level='INFO')
    except NameError:
        logging.info('Logging framework setup did not work falling back to basic config')
        logging.basicConfig(format=fmt, datefmt=date_fmt, level='INFO')


def load_to_snowflake(cma_df: pd.DataFrame):
    try:
        engine_url = 'snowflake://' + os.environ['SNOWFLAKE_USERNAME'] + ':' + os.environ['SNOWFLAKE_PASSWORD'] + '@' + \
                     os.environ['SNOWFLAKE_ACCOUNT'] + '/'

        engine = create_engine(engine_url.format(database='SANDBOX'))

        with engine.begin() as connection:
            connection.execute("USE SANDBOX")
            results = connection.execute('select current_version()').fetchone()
            connection.execute("drop table if exists cma_shapes;")
            print(results[0])
            cma_df.columns = cma_df.columns.str.upper()
            cma_df = cma_df.astype(str)
            cma_df.to_sql('cma_shapes', connection, index=False, method=pd_writer, if_exists='append',
                          dtype={'CMA': sqlalchemy.types.INTEGER,
                                 'NAME': sqlalchemy.types.NVARCHAR(length=256)})

        connection.close()
        engine.dispose()
    except Exception as ex:
        logging.error(f'''Dataframe not loaded to snowflake, exception:
        :{str(ex)}''')


def main():
    logging.info('Loading shapefile from an mid file')
    cma = gpd.read_file("cma/cma.mid")
    logging.info(f'''A snippet of the file contents as a geopandas dataframe:
{cma.head()}''')

    cmadf = pandas.DataFrame(cma)
    logging.info(f'Converted the geopandas dataframe to a regular pandas dataframe, row count:{cmadf.shape[0]}')
    try:
        cmadfgeo = gpd.GeoDataFrame(cmadf, crs="EPSG:4326", geometry=cmadf['geometry'])
        logging.info('The dataframe was  successfully turned back into a geopandas dataframe for validation, shapes:')
        logging.info(f'''
{cmadfgeo.geom_type.value_counts()}
        ''')
        load_to_snowflake(cma_df=cmadf)
        logging.info('!!!Dataframe loaded into Snowflake!!!!')

    except Exception as ex:
            logging.error(f'''The dataframe was not successfully turned into a geopandas dataframe, exception:
            {str(ex)}''')


if __name__ == "__main__":
    setup_logger()
    logging.warning('Process for loading CMA files has started')
    main()
