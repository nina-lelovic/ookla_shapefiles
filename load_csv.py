import os

import pandas as pd
import sqlalchemy
from pyreadstat import read_sas7bdat

from dotenv import load_dotenv
from snowflake.connector.pandas_tools import pd_writer
from sqlalchemy import engine, create_engine

load_dotenv()

def main():


    try:

        df = pd.read_csv('sampel_csv_file.csv', encoding='windows-1252')
        engine_url = 'snowflake://' + os.environ['SNOWFLAKE_USERNAME'] + ':' + os.environ['SNOWFLAKE_PASSWORD'] + '@' + \
                     os.environ['SNOWFLAKE_ACCOUNT'] + '/'

        engine = create_engine(engine_url.format(database='SANDBOX'))

        with engine.begin() as connection:
            connection.execute("USE SANDBOX")
            results = connection.execute('select current_version()').fetchone()
            # connection.execute("drop table if exists xxx;")
            print(results[0])
            df.columns = df.columns.str.upper()
            df = df.astype(str)
            df.to_sql(('sample_table_name').upper(), connection, index=False, method=pd_writer,
                      if_exists='append')
    except Exception as ex:
        print(str(ex))

def execute_query(connection, query):
    try:
        cursor = connection.cursor()
        cursor.execute(query)
        cursor.close()
    except Exception as ex:
        print(str(ex))



if __name__ == "__main__":
    print("Warning process has started")
    main()
