import datetime
import os
import pandas as pd
from dotenv import load_dotenv
from sqlalchemy import engine, create_engine
import geopandas as gpd
from shapely import wkt
from shapely.geometry import  Point

load_dotenv()

def main():

    try:
        engine_url = 'snowflake://' + os.environ['SNOWFLAKE_USERNAME'] + ':' + os.environ['SNOWFLAKE_PASSWORD'] + '@' + \
                     os.environ['SNOWFLAKE_ACCOUNT'] + '/'
        engine = create_engine(engine_url.format(database='SANDBOX'))

        with engine.begin() as connection:
            connection.execute("USE SANDBOX")
            shapes_df = pd.read_sql_query(sql='select * from cma_shapes', con=connection)
            shapes_geodf = shapes_df
            shapes_geodf['geometry'] = shapes_geodf['geometry'].apply(wkt.loads)
            shapes_geodf = gpd.GeoDataFrame(shapes_geodf, crs="EPSG:4326", geometry=shapes_geodf['geometry'])
            shape_bounds = shapes_geodf.bounds
            shapes_df_bounds = shapes_geodf.join(shape_bounds)

            gini_df = pd.read_sql_query(sql='select CLIENT_LATITUDE, CLIENT_LONGITUDE,CMA from GINIFIED_TABLE_NEW', con=connection)
            gini_df['client_latitude'] = gini_df['client_latitude'].astype('float')
            gini_df['client_longitude'] = gini_df['client_longitude'].astype('float')
            gini_df['test_cma'] = 0
            start_time = print(datetime.datetime.now())
            print(start_time)
            for i in range(10000):
                # print(i)
                lat = gini_df['client_latitude'][i]
                long = gini_df['client_longitude'][i]

                bbox_test = shape_bounds.loc[(shape_bounds['miny'] <= lat) & (shape_bounds['maxy'] >= lat) & (shape_bounds['minx'] <= long) & (shape_bounds['maxx'] >= long)]
                if bbox_test.shape[0] == 1:
                    gini_df.loc[i, 'test_cma'] = bbox_test['cma'].values[0]
                if bbox_test.shape[0] == 0:
                    print('no shapes found')
                if bbox_test.shape[0] > 1:
                    shape_test =bbox_test.contains(Point(long,lat))
                    if bbox_test[shape_test.values]['cma'].shape[0] ==1:
                        gini_df.loc[i, 'test_cma'] =bbox_test[shape_test.values]['cma'].values[0]
            gini_df.to_csv(path_or_buf='test_run_gini.csv',index=False)
            print('done')
    except Exception as ex:
        print(str(ex))

def execute_query(connection, query):
    try:
        cursor = connection.cursor()
        cursor.execute(query)
        cursor.close()
    except Exception as ex:
        print(str(ex))


if __name__ == "__main__":
    print("Warning process has started")
    main()
