FCC Areas
http://www.fcc.gov/oet/info/maps/areas/

This mapping data was compiled from 1990 Census data
obtained from ftp://ftp.census.gov/pub/tiger/boundary/

This data is in MapInfo Interchange Format which can be
imported into a variety of Geographic Information Systems.

  *.mif MIF file part 1 (contains ASCII geographic data)
  *.mid MIF file part 2 (contains ASCII associated data)
  *.tab,*.dat,*.id,*.map (contains binary MapInfo Native Format geographic data)
  *.wor MapInfo Workspace

MapInfo Instruction on use.
  Load Workspace
