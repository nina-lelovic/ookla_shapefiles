import os

import numpy as np
import pandas
import geopandas as gpd
import coloredlogs, logging
import sqlalchemy
from snowflake.connector.pandas_tools import pd_writer
from dotenv import load_dotenv
from sqlalchemy import create_engine



import sqlite3
import pandas as pd
from tqdm import tqdm

load_dotenv()


def chunker(seq, size):
    # from http://stackoverflow.com/a/434328
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))


def setup_logger():
    fmt = '%(asctime)s [%(levelname)s] %(message)s'
    date_fmt = '%Y-%m-%d %H:%M'
    try:
        fmt = '%(asctime)s [%(levelname)s] %(message)s'
        date_fmt = '%Y-%m-%d %H:%M'
        coloredlogs.install(fmt=fmt, datefmt=date_fmt, level='INFO')
    except NameError:
        logging.info('Logging framework setup did not work falling back to basic config')
        logging.basicConfig(format=fmt, datefmt=date_fmt, level='INFO')


def load_to_snowflake(usa_zips_df: pd.DataFrame):
    try:
        engine_url = 'snowflake://' + os.environ['SNOWFLAKE_USERNAME'] + ':' + os.environ['SNOWFLAKE_PASSWORD'] + '@' + \
                     os.environ['SNOWFLAKE_ACCOUNT'] + '/'

        engine = create_engine(engine_url.format(database='SANDBOX'))

        with engine.begin() as connection:
            connection.execute("USE SANDBOX")
            results = connection.execute('select current_version()').fetchone()
            connection.execute("drop table if exists usa_zips_shapes;")
            logging.info(f'Current version of Snowflake:{results}')
            usa_zips_df.columns = usa_zips_df.columns.str.upper()
            # usa_zips_df = usa_zips_df.astype(str)

            usa_zips_df = usa_zips_df.astype('str')
            usa_zips_df.to_sql('usa_zips_shapes', connection, index=False,
                               method='multi',
                               if_exists='append',
                               dtype={'GEOID10': sqlalchemy.types.NVARCHAR(length=256),
                                      'FUNCSTAT10': sqlalchemy.types.NVARCHAR(length=256),
                                      'CLASSFP10': sqlalchemy.types.NVARCHAR(length=256),
                                      'INTPTLON10': sqlalchemy.types.NVARCHAR(length=256),
                                      'AWATER10': sqlalchemy.types.NVARCHAR(length=256),
                                      'ALAND10': sqlalchemy.types.NVARCHAR(length=256),
                                      'MTFCC10': sqlalchemy.types.NVARCHAR(length=256),
                                      'ZCTA5CE10': sqlalchemy.types.NVARCHAR(length=256),
                                      'INTPTLAT10': sqlalchemy.types.NVARCHAR(length=256)})

        connection.close()
        engine.dispose()
    except Exception as ex:
        logging.error(f'''Dataframe not loaded to snowflake, exception:
        :{str(ex)}''')


def main():
    logging.info('Loading shapefile from an shp file')
    usa_zips = gpd.read_file("data/tl_2010_us_zcta510/tl_2010_us_zcta510.shp")
    logging.info(f'''A snippet of the file contents as a geopandas dataframe:
{usa_zips.head()}''')

    usa_zipsdf = pandas.DataFrame(usa_zips)
    logging.info(f'Converted the geopandas dataframe to a regular pandas dataframe, row count:{usa_zipsdf.shape[0]}')
    for i in range(1, 1):
        usa_zipsdf_snippit = usa_zipsdf.iloc[np.r_[(i*100+1):(i*100+100)]]

        try:
            usa_zipsdfgeo = gpd.GeoDataFrame(usa_zipsdf_snippit, crs="EPSG:4326", geometry=usa_zipsdf_snippit['geometry'])
            logging.info('The dataframe was  successfully turned back into a geopandas dataframe for validation, shapes:')
            logging.info(f'''
    {usa_zipsdfgeo.geom_type.value_counts()}
            ''')
            load_to_snowflake(usa_zips_df=usa_zipsdf_snippit)
            logging.info('!!!Dataframe loaded into Snowflake!!!!')
            print(f'this is batch:{i}')

        except Exception as ex:
            logging.error(f'''The dataframe was not successfully turned into a geopandas dataframe, exception:
                {str(ex)}''')


if __name__ == "__main__":
    setup_logger()
    logging.warning('Process for loading usa_zips files has started')
    main()
#330